using UnityEngine;
using System.Collections;

public class GOverSceneAction : MonoBehaviour {
	public GUITexture tex;
	private float alpha = 0;
	public Texture[] num = new Texture[10];
	
	// Update is called once per frame
	void Update () {
		alpha += (Time.deltaTime / 10);
		alpha = Mathf.Clamp(alpha, 0.0f, 0.5f);
		tex.color = new Color(0.5f, 0.5f, 0.5f, alpha);
	}
	
	
	void OnGUI()
	{
		GUI.matrix = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3(Screen.width / 640f, Screen.height / 480f, 1));
		
		//RESTARTボタン.
		if(GUI.Button(new Rect(100, 350, 150, 60), "RESTART" ))
		{
			Application.LoadLevel("Title");
			Destroy(GameObject.Find ("Player"));
		}
		
		//ENDボタン.
		if(GUI.Button(new Rect(390, 350, 150, 60), "END" ))
		{
			Application.Quit();
		}
		
		//スコア
		int score = GameObject.Find ("Player").GetComponent<PlayerAction>().score;
		for(int i = 0; i < 5; i++)
		{
			GUI.DrawTexture(new Rect( 369 - i * 33.0f, 280.0f, 41.0f, 54.0f), num[score % 10]);
			score /= 10;
		}		
	}
}
