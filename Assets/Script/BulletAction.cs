using UnityEngine;
using System.Collections;

public class BulletAction : MonoBehaviour {
	public float speed = 30.0f;
	public GameObject preBom;
	
	void Start(){
		
	}

	void Update () {
		transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
	}
	
	void  OnTriggerEnter(Collider info){
		if(info.gameObject.tag == "Enemy")
		{
			if(info.GetComponent<EnemyAction>().onFlag == true)
			{
				Instantiate(preBom, this.transform.position, this.transform.rotation);
				Destroy (this.gameObject);
			
				info.SendMessage("Hit");
			}
		}
	}
	
	
	void OnBecameInvisible()
	{
		Destroy (this.gameObject);	
	}
	
	
}
