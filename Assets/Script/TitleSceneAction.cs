using UnityEngine;
using System.Collections;

public class TitleSceneAction : MonoBehaviour {

	void OnGUI()
	{
		GUI.matrix = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3(Screen.width / 640f, Screen.height / 480f, 1));
		
		//STARTボタン.
		if(GUI.Button(new Rect(245, 380, 150, 60), "START" ))
		{
			Application.LoadLevel("Play");
		}

	}
}
