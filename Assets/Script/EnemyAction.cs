using UnityEngine;
using System.Collections;

[System.Serializable]
public class EnemyAction : MonoBehaviour {
	private GameObject cam;
	private GameObject player;
	GameObject dummy;
	public GameObject preBom; 
	public GameObject preBullet; 
	public AudioClip seBom;
	public AudioClip seHit;
	
	public int hp = 3;				//耐久力.
	public float shotTime = 1.0f;	//発射間隔.
	public float shotAngle = 0.0f;	//発射角度.
	public int shotNum = 1;			//発射量.
	public float shotSpeed = 50.0f;	//弾速度.
	public int loop = 0;
	public bool isDeath = false;		//死亡フラグ.
	
	private int movePattern = 0;	//現在の移動パターン.
	private float moveChangeTime = 0;	//移動パターンを切り替えた時の時間.
	public bool onFlag = false;			//ゲームに登場しているかどうか.
	
	private Vector3 startPos;	//初期位置.
	private int startHp;		//初期HP.
	
	
	//動きパターン.
	[System.Serializable]
	public class MoveData{
		[SerializeField]public float angle = 0;
		[SerializeField]public float speed = 10;
		[SerializeField]public float time = 0;
	};
	
	[SerializeField]
	public MoveData[] move = new MoveData[5];
	
	
	void Start() {
		player = GameObject.Find("Player");
		cam = GameObject.Find("Camera");
		dummy = GameObject.Find("Dummy");
		onFlag = false;
		startPos = this.transform.position;
		startHp = hp;
		if(shotNum > 0)	InvokeRepeating("Shot", 1.0f, shotTime);
	}

	
	
	// Update is called once per frame
	void Update () {
		if(isDeath)	return;
		
		//待機.
		if(this.transform.position.z - cam.transform.position.z > 200)
		{
			moveChangeTime = Time.time;
			return;
		}
		
		onFlag = true;
		
		//移動.
		{
			//ダミーオブジェクトを移動方向に向ける.
			dummy.transform.eulerAngles = new Vector3(0.0f, move[movePattern].angle, 0.0f );
			
			//移動ベクトル.
			Vector3 v = dummy.gameObject.transform.forward * -move[movePattern].speed;
	
			//移動.
			this.transform.position += (v * Time.deltaTime);
		}
		
		//移動パターン切り替え.
		if(moveChangeTime + move[movePattern].time <= Time.time)
		{
			moveChangeTime = Time.time;
			movePattern++;
			if( movePattern >= move.Length || move[movePattern].time <= 0)
				movePattern = loop;
		}
		
		
		//ずっと回転.
		transform.Rotate(Vector3.up, 3.0f);
	}
	
	
	void Hit()
	{
		GetComponent<AudioSource>().clip = seHit;
		GetComponent<AudioSource>().Play();	
		hp--;
		if(hp <= 0){
			Instantiate(preBom, this.transform.position, this.transform.rotation);
			Kill(true);
		}
	}
	
	void Shot()
	{
		if(isDeath)	return;
		if(!onFlag)	return;
		
        float angle;
        if (shotNum != 1)
            angle = (float)(shotAngle * 2) / (shotNum - 1);    //弾と弾の角度
        else
            angle = 0;

        for (int j = 0; j < shotNum; j++)
        {
            float l = -shotAngle + angle * j;
			GameObject b = (GameObject)Instantiate(preBullet, this.transform.position, Quaternion.Euler(0, 180, 0));
			
			dummy.transform.eulerAngles = new Vector3(0.0f, l, 0.0f );
			b.GetComponent<EneBulletAction>().move = dummy.transform.forward * shotSpeed;
        }
	}
	
	void OnBecameInvisible()
	{
		if(onFlag == true)
			Kill(false);
	}
	
	void  OnTriggerEnter(Collider info){
		if(isDeath)	return;
		
		if(info.gameObject.name == "Player")
		{
			if(info.GetComponent<PlayerAction>().clash == false)
			{
				info.SendMessage("Hit");
			}
		}
	}
	
	void Kill(bool hit)
	{
		//ちゃんと倒した（≠画面外に消えた）.
		if(hit)
		{
			GetComponent<AudioSource>().clip = seBom;
			GetComponent<AudioSource>().Play();
	
			if(player)
			{
				float dist = Vector3.Distance(this.transform.position, player.transform.position);
				player.SendMessage("AddScore", dist);
			}
		}
		
		isDeath = true;
		onFlag = false;
		this.transform.localScale = new Vector3(0, 0, 0);
	}
	
	void Reset()
	{
		GetComponent<AudioSource>().clip = null;
		transform.position = new Vector3(startPos.x, 0.0f, cam.transform.position.z + 205 + startPos.z);
		this.transform.localScale = new Vector3(1, 1, 1);
		hp = startHp;
		onFlag = false;
		isDeath = false;
		movePattern = 0;
		moveChangeTime = 0;
		
		//パワーアップ.
		hp += 1;
		startHp = hp;
		shotTime *= 0.8f;
		shotNum += 1;
		
		
		for(int i = 0; i < move.Length; i++)
		{
			move[i].speed *= 1.2f;
			move[i].time /= 1.2f;
		}
	}
}
