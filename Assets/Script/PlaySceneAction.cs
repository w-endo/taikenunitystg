using UnityEngine;
using System.Collections;

//プレイシーンの処理.
public class PlaySceneAction : MonoBehaviour 
{
	private GameObject Enemys;

	// 最初の処理.
	void Start () 
	{
		//敵のフォルダ.
		Enemys = GameObject.Find("Enemys");
	}
	
	// 毎フレーム行う処理.
	void Update () 
	{
		//敵の数.
		int eneNum = Enemys.transform.childCount;
		
		//倒した敵の数.
		int deathCnt = 0;
		for(int i = 0; i < eneNum; i++)
		{
			Transform child = Enemys.transform.GetChild (i);
			if(child.GetComponent<EnemyAction>().isDeath)
				deathCnt++;
		}
		
		//すべて倒した場合.
		if(deathCnt == eneNum)
		{
			//敵レベルアップ.
			Invoke("LevelChange", 0);
		}
	}
	
	//敵レベルアップ.
	void LevelChange()
	{
		int eneNum = Enemys.transform.childCount;
		for(int i = 0; i < eneNum; i++)
		{
			print (i);
			Transform child = Enemys.transform.GetChild (i);
			child.SendMessage("Reset");
		}
	}
}
