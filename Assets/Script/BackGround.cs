using UnityEngine;
using System.Collections;

public class BackGround : MonoBehaviour {
	public GameObject preStar;
	GameObject backGround;
	public int STAR_NUM = 500;
	GameObject cam;

	// Use this for initialization
	void Start () {
		backGround = GameObject.Find("BackGroundStar");
		cam = GameObject.Find("Camera");
		
		for (int i = 0; i < STAR_NUM; i++)
		{
			Vector3 pos;
            while (true)
            {
                pos = new Vector3(Random.Range(-60.0f, 60.0f), Random.Range (-30.0f, 30.0f), Random.Range(-200.0f, 0.0f));
                if (pos.x < -7.0f || pos.x > 7.0f || pos.y < -10.0f || pos.y > 10.0f)
                    break;
            }
			GameObject star = (GameObject)Instantiate(preStar, pos, this.transform.rotation);
			star.transform.parent = backGround.transform;
		}
	}

	void Update () {
		int ChildCount = this.transform.childCount;
		for(int i = 0; i < ChildCount; i++){
    		Transform child = transform.GetChild (i);
    		child.Translate(0.0f, 0.0f, -1.0f);
			
			if(child.position.z < cam.transform.position.z)
			{
				child.position = new Vector3(child.position.x, child.position.y, cam.transform.position.z + 200.0f);
			}
		}
	}
}
