using UnityEngine;
using System.Collections;


//自機の処理.
public class PlayerAction : MonoBehaviour {

	private GameObject cam;			//カメラオブジェクト.
	private GameObject bullets;		//弾の親オブジェクト.
		
	//サウンド.
	public AudioClip seShot;	//ショット.
	public AudioClip seHit;		//ヒット.
	public AudioClip seBom;		//爆発.
	
	//プレハブ.
	public GameObject preBom; 		//爆発プレハブ.
	public GameObject preBullet;	//弾のプレハブ.
	
	
	//パラメータ.
	private float playerX, preX;	//自機の位置.
	public bool clash = false;		//点滅中.
	public int scale;				//点滅中のサイズ.
	public float ShotTime = 0.6f;	//弾を打つ間隔（秒）.
	public int moveDelay = 20;		//移動のラグ.
	public int hp = 3;				//自機のライフ.
	public int score = 0;			//スコア.
	bool onFlag = true;

	
	
	//最初に呼ばれる関数.
	void Start () {
		DontDestroyOnLoad(this);				//シーンの切り替え時削除しない.		
		cam = GameObject.Find("Camera");		//カメラ.
		bullets = GameObject.Find("Bullets");	//弾の親オブジェクト.
		


		//定期的に弾を撃つ.	
        InvokeRepeating("Shot", 2.0f, 0.5f);
		
				
		
		
	}
	

	//毎フレーム呼ばれる関数.
	void Update () {
		if(onFlag == false)	return;
		
		//タッチ位置
		float touchX = Input.mousePosition.x / Screen.width * 100.0f;
		



		
		//自機の位置.
        playerX = (touchX - 50) / 10;
		

		
		
		//自機移動.
		this.transform.position = new Vector3(((playerX*2.0f)+this.transform.position.x *(moveDelay-1))/moveDelay, 0, -200 + Time.timeSinceLevelLoad + 20.0f);
		
		//傾き.
		float angle = (preX - this.transform.position.x) * 200;
		angle = Mathf.Clamp(angle, -45, 45);
		transform.eulerAngles = new Vector3(0.0f, 0.0f, angle);
		preX = this.transform.position.x;
		
		//カメラ移動.
		cam.transform.position = new Vector3(0, 7.0f, this.transform.position.z - 24.0f);
	}
	
	
	//弾を撃つ.
	void Shot()
	{
		if(this.gameObject.active == false)	return;
		GameObject bullet = 
			(GameObject)Instantiate(preBullet, this.transform.position + new Vector3(0, 0, 1), this.transform.rotation);
		bullet.transform.parent = bullets.transform;
	}
	
	
	//敵の弾に当たった時の処理.
	void Hit()
	{
		if(onFlag == false)	return;
		
		//ダメージ.
		hp--;
		
		//ヒット音.
		GetComponent<AudioSource>().clip = seHit;
		GetComponent<AudioSource>().Play();		
		
		if(hp < 0){
			//ゲームオーバー.
			onFlag = false;
			GetComponent<AudioSource>().clip = seBom;
			GetComponent<AudioSource>().Play();	
			CancelInvoke("Shot");
			Instantiate(preBom, this.transform.position, this.transform.rotation);
			Invoke ("GameOver", 5);
			transform.localScale = new Vector3(0,0,0);
			
		}else{
			//クラッシュ.
			clash = true;
			InvokeRepeating("Flash", 0f, 0.05f);
			Invoke("NoClash",2);
		}
	}
	
	//点滅.
	void Flash()
	{
		if(onFlag == false)	return;
		
				



		//scaleを 0⇔1 切り替える.
        scale = 1 - scale;
			
		
		
		
		//自機のサイズをscale倍にする.
		transform.localScale = new Vector3(scale,scale,scale);	
	}
	
	
	//復帰処理.
	void NoClash()
	{
		if(onFlag == false)	return;
		
		//点滅をやめる.
		clash = false;
		CancelInvoke("Flash");
		this.transform.localScale = new Vector3(1,1,1);
	}
	
	//ゲームオーバー.
	void GameOver()
	{
		Application.LoadLevel("GameOver");
	}
	
	//スコア加算.
	void AddScore(float f)
	{
		if(onFlag == false)	return;
		score += (int)f;
	}
}
