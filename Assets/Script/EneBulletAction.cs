using UnityEngine;
using System.Collections;

public class EneBulletAction : MonoBehaviour {

	public float speed = 30.0f;
	public GameObject preBom;
	public Vector3 move;

	void Update () {
		
		transform.Translate(move * Time.deltaTime);
	}
	
	void  OnTriggerEnter(Collider info){
		if(info.gameObject.name == "Player")
		{
			if(info.GetComponent<PlayerAction>().clash == false)
			{
				Instantiate(preBom, this.transform.position, this.transform.rotation);
				Destroy (this.gameObject);
				info.SendMessage("Hit");
			}
		}
	}
	
	
	void OnBecameInvisible()
	{
		Destroy (this.gameObject);	
	}
}
