using UnityEngine;
using System.Collections;

public class DrawGui : MonoBehaviour {

	public Texture statusBar;
	public Texture[] life = new Texture[4];
	public Texture[] num = new Texture[10];
	GameObject player;
	int drawScore;
	
	void Start()
	{
		player = GameObject.Find("Player");
		drawScore = 0;
	}
	
	
	
	void OnGUI()
	{
		GUI.matrix = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3(Screen.width / 1360f, Screen.height / 765f, 1));
		
		//ステータスバー.
		GUI.DrawTexture(new Rect(0, 0, 1360, 66), statusBar);
		
		//ライフ.
		int l = 0;
		if(player != null)	l = player.GetComponent<PlayerAction>().hp;
        if (l < 0)	l = 0;
		GUI.DrawTexture(new Rect(970.0f, 10.0f, 400f, 46f), life[l]);
		
		//スコア
		int score = player.GetComponent<PlayerAction>().score;
		if(drawScore < score) drawScore++;
		score = drawScore;
		for(int i = 0; i < 5; i++)
		{
			GUI.DrawTexture(new Rect( 320.0f - i * 33.0f, 5.0f, 41.0f, 54.0f), num[score % 10]);
			score /= 10;
		}
	}	
	
}
